package lt.carleasing.requests.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Set;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FundingRequestInput {
    private Integer applicantId;
    private double fundingAmount;
    private Set<Integer> coApplicantsIds;
    private String vinNumber;
}
