package lt.carleasing.requests.model;

import lombok.Data;
import lt.carleasing.persons.Person;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@Audited
@Table(name = "requests")
public class FundingRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer requestId;

    @ManyToOne
    private Person applicant;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "Requests_Applicants",
            joinColumns = {@JoinColumn(name = "person_id")},
            inverseJoinColumns = {@JoinColumn(name = "request_id")}
    )
    private Set<Person> coApplicants;

    private Double amount = (double) 0;

    private String vinNumber;

    private FundingRequestStatus decisionStatus;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    private String createdBy = "user";

}
