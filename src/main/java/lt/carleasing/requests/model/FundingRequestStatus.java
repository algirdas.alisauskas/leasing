package lt.carleasing.requests.model;

public enum FundingRequestStatus {
    APPROVED, REJECTED
}
