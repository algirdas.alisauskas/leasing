package lt.carleasing.requests;

import lt.carleasing.persons.Person;
import lt.carleasing.requests.model.FundingRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FundingRequestsRepository extends JpaRepository<FundingRequest, Integer> {

    List<FundingRequest> getAllByApplicant(Person applicant);

}
