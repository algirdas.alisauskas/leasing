package lt.carleasing.requests;

import lombok.AllArgsConstructor;
import lt.carleasing.persons.PersonService;
import lt.carleasing.requirements.IncomesService;
import lt.carleasing.requirements.MinimalIncome;
import lt.carleasing.persons.Person;
import lt.carleasing.requests.model.FundingRequest;
import lt.carleasing.requests.model.FundingRequestInput;
import lt.carleasing.requests.model.FundingRequestStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
@Transactional
public class FundingRequestsService {
    private final FundingRequestsRepository repository;
    private final IncomesService incomesService;
    private final PersonService personService;

    public FundingRequest submit(FundingRequestInput input) {

        FundingRequest request = new FundingRequest();
        request.setApplicant(personService.getPersonBy(input.getApplicantId()));

        if (input.getCoApplicantsIds() != null) {
            Set<Person> coApplicants = input
                    .getCoApplicantsIds()
                    .stream()
                    .map(personService::getPersonBy)
                    .collect(Collectors.toSet());
            request.setCoApplicants(coApplicants);
            request.setAmount(
                    coApplicants
                            .stream()
                            .map(Person::getIncome)
                            .mapToDouble(Double::doubleValue)
                            .sum());
        }
        request.setAmount(
                request.getAmount()
                        + request.getApplicant().getIncome());
        request.setVinNumber(input.getVinNumber());

        MinimalIncome minimalIncome = incomesService.getActualIncome();

        if (request.getAmount().compareTo(minimalIncome.getSum()) >= 0) {
            request.setDecisionStatus(FundingRequestStatus.APPROVED);
        } else {
            request.setDecisionStatus(FundingRequestStatus.REJECTED);
        }

        return repository.save(request);
    }

    public List<FundingRequest> getFoundingRequestsByApplicant(Person applicant) {
        return repository.getAllByApplicant(applicant);
    }

    public FundingRequest getFundingRequestById(Integer requestId) {
        return repository.getOne(requestId);
    }
}
