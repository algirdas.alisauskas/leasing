package lt.carleasing.requests;

import lombok.AllArgsConstructor;
import lt.carleasing.requests.model.FundingDecision;
import lt.carleasing.requests.model.FundingRequestsResponse;
import lt.carleasing.requests.model.FundingRequest;
import lt.carleasing.requests.model.FundingRequestInput;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/requests")
@AllArgsConstructor
public class FundingRequestsController {
    private final FundingRequestsService requestsService;

    @PostMapping
    public FundingRequestsResponse submit(@RequestBody FundingRequestInput input) {
        return new FundingRequestsResponse(buildFundingDecision(
                requestsService.submit(input)
        ));
    }

    @GetMapping("/{requestId}")
    public FundingRequestsResponse getRequestById(@PathVariable Integer requestId) {
        return new FundingRequestsResponse(buildFundingDecision(
                requestsService.getFundingRequestById(requestId)
        ));
    }

    private FundingDecision buildFundingDecision(FundingRequest request) {
        return FundingDecision
                .builder()
                .requestId(request.getRequestId())
                .fundingAmount(request.getAmount())
                .requestStatus(request.getDecisionStatus())
                .vinNumber(request.getVinNumber())
                .build();
    }

}
