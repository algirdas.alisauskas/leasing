package lt.carleasing.requirements;

import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "incomes")
@Audited
public class MinimalIncome {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private double sum;

    private boolean actual;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    private String createdBy = "user";
}
