package lt.carleasing.requirements;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
public class IncomesService {

    private final IncomesRepository incomesRepository;

    public MinimalIncome getActualIncome() {
        return incomesRepository.getByActualTrue().stream().findFirst().orElseThrow();
    }

    @Transactional
    public void createMinimalIncome(MinimalIncomeInput input) {
        incomesRepository.disableActualIncomes();

        MinimalIncome incomes = new MinimalIncome();
        incomes.setSum(input.getSum());
        incomes.setActual(true);

        incomesRepository.save(incomes);
    }
}
