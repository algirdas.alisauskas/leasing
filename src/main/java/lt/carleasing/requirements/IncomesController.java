package lt.carleasing.requirements;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Data
@RestController
@RequestMapping("/incomes")
public class IncomesController {
    private final IncomesService incomesService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void setMinimalIncome(@RequestBody MinimalIncomeInput input) {
        incomesService.createMinimalIncome(input);
    }
}
