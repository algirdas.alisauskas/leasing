package lt.carleasing.requirements;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncomesRepository extends JpaRepository<MinimalIncome, Integer> {

    List<MinimalIncome> getByActualTrue();

    @Modifying
    @Query("update MinimalIncome income set income.actual = false where income.actual = true")
    void disableActualIncomes();

}
