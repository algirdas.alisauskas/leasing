package lt.carleasing.persons;

import lombok.AllArgsConstructor;
import lt.carleasing.persons.model.ApplicantInput;
import lt.carleasing.persons.model.PersonResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/persons")
@AllArgsConstructor
public class PersonController {
    private final PersonService personService;

    @GetMapping("/{personCode}")
    public PersonResponse getPersonBy(@PathVariable String personCode) {
        Person person = personService.getPersonBy(personCode);
        if (person != null) {
            return new PersonResponse(person.getPersonId());
        }
        return new PersonResponse();
    }

    @PostMapping
    public PersonResponse saveAndCreatePerson(@RequestBody ApplicantInput input) {
        return new PersonResponse(personService.createPersonAndSave(input).getPersonId());
    }
}
