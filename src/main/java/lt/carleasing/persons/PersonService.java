package lt.carleasing.persons;

import lombok.AllArgsConstructor;
import lt.carleasing.persons.model.ApplicantInput;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;

    public Person getPersonBy(String personCode) {
        return personRepository.findByPersonCode(personCode);
    }

    public Person getPersonBy(Integer personId) {
        return personRepository.getOne(personId);
    }

    public Person createPersonAndSave(ApplicantInput input) {
        return personRepository.save(createPersonFrom(input));
    }

    private Person createPersonFrom(ApplicantInput applicant) {
        Person person = new Person();
        person.setName(applicant.getName());
        person.setIncome(applicant.getIncome());
        person.setPersonCode(applicant.getPersonCode());
        return person;
    }
}
