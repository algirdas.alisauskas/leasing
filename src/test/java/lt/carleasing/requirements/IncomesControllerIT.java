package lt.carleasing.requirements;

import lt.carleasing.CarLeasingApplicationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class IncomesControllerIT extends CarLeasingApplicationTest {
    private static final String SERVER_ADDRESS = "http://localhost:8081";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void setMinimalIncome_shouldCreateSuccessful(){
        MinimalIncomeInput input = new MinimalIncomeInput(700);

        ResponseEntity<String> response =
                this.restTemplate.postForEntity(SERVER_ADDRESS+"/incomes", input, String.class);

        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
    }

}
