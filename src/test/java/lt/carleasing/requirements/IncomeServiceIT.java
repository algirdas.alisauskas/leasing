package lt.carleasing.requirements;

import lt.carleasing.CarLeasingApplicationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class IncomeServiceIT extends CarLeasingApplicationTest {

    @Autowired
    private IncomesService incomesService;

    @Test
    public void createMinimalIncome_ShouldDisableOldValuesAndCreateNewOne() {
        incomesService.createMinimalIncome(new MinimalIncomeInput(700));
        incomesService.createMinimalIncome(new MinimalIncomeInput(600));

        MinimalIncome actualIncome = incomesService.getActualIncome();

        assertEquals(600, actualIncome.getSum());
    }
}
