package lt.carleasing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.stereotype.Service;

@Component
public class RestUtil {

    private static final String SERVER_ADDRESS = "http://localhost:8081";

    @Autowired
    private TestRestTemplate restTemplate;

    public <T> T postForObject(String controller, Object object, Class<T> response) {
        return this.restTemplate
                .postForObject(SERVER_ADDRESS + controller, object, response);
    }

    public <T> T getForObject(String controller, Class<T> response, Object pathVariable) {
        return this.restTemplate
                .getForObject(
                        SERVER_ADDRESS + controller + "/" +pathVariable,
                        response);
    }
}
