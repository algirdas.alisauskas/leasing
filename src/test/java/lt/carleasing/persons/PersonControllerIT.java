package lt.carleasing.persons;

import lt.carleasing.CarLeasingApplicationTest;
import lt.carleasing.RestUtil;
import lt.carleasing.persons.model.ApplicantInput;
import lt.carleasing.persons.model.PersonResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class PersonControllerIT extends CarLeasingApplicationTest {

    @Autowired
    private RestUtil restUtil;

    @Test
    public void getPersonBy_ShouldReturnEmptyResponse() {
        var personCode = "P001";

        PersonResponse response
                = restUtil
                .getForObject("/persons", PersonResponse.class, personCode);

        assertNull(response.getPersonId());
    }

    @Test
    public void saveAndCreatePerson_ShouldReturnCreatedPerson() {
        ApplicantInput input = ApplicantInput
                .builder()
                .income(700)
                .name("Mike")
                .personCode("P001")
                .build();

        PersonResponse response
                = restUtil
                .postForObject("/persons", input, PersonResponse.class);


        assertNotNull(response.getPersonId());
    }

}