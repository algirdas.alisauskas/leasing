package lt.carleasing.persons;

import lt.carleasing.persons.model.ApplicantInput;
import lt.carleasing.CarLeasingApplicationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class PersonServiceIT extends CarLeasingApplicationTest {

    @Autowired
    private PersonService personService;

    @Test
    public void createAnSavePerson_ShouldCreateNewPerson(){
        var applicant =
                new ApplicantInput(
                        "Jasmine", 400, "CODE001");

        Person person = personService.createPersonAndSave(applicant);

        assertNotNull(person);
    }

    @Test
    public void getPersonBy_ShouldReturnPersonByPersonCode(){
        var applicant =
                new ApplicantInput(
                        "Jasmine", 400, "CODE001");
        Person person = personService.createPersonAndSave(applicant);

        Person theSamePerson = personService.getPersonBy("CODE001");

        assertEquals(person, theSamePerson);
    }
}
