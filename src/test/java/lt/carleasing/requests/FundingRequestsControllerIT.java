package lt.carleasing.requests;

import lt.carleasing.CarLeasingApplicationTest;
import lt.carleasing.RestUtil;
import lt.carleasing.persons.model.ApplicantInput;
import lt.carleasing.persons.model.PersonResponse;
import lt.carleasing.requests.model.FundingRequestInput;
import lt.carleasing.requests.model.FundingRequestStatus;
import lt.carleasing.requests.model.FundingRequestsResponse;
import lt.carleasing.requirements.MinimalIncomeInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class FundingRequestsControllerIT extends CarLeasingApplicationTest {

    @Autowired
    private RestUtil restUtil;

    @BeforeEach
    public void init(@Autowired RestUtil restUtil) {
        restUtil.postForObject("/incomes", new MinimalIncomeInput(600), String.class);
    }

    @Test
    public void submit_shouldReturnDecisionWithApprovedStatus() {
        ApplicantInput applicant = ApplicantInput
                .builder()
                .income(700)
                .name("Mike")
                .personCode("P001")
                .build();
        ApplicantInput cooApplicant = ApplicantInput
                .builder()
                .income(700)
                .name("Jose")
                .personCode("P002")
                .build();
        PersonResponse person1
                = restUtil.postForObject("/persons", applicant, PersonResponse.class);
        PersonResponse person2
                = restUtil.postForObject("/persons", cooApplicant, PersonResponse.class);
        FundingRequestInput input = FundingRequestInput
                .builder()
                .applicantId(person1.getPersonId())
                .coApplicantsIds(Set.of(person2.getPersonId()))
                .fundingAmount(5500)
                .vinNumber("123456789")
                .build();

        FundingRequestsResponse response
                = restUtil.postForObject("/requests", input, FundingRequestsResponse.class);

        assertEquals(FundingRequestStatus.APPROVED, response.getDecision().getRequestStatus());
    }

    @Test
    public void getRequestById_shouldReturnDecisionWithRejectedStatus() {
        ApplicantInput applicant = ApplicantInput
                .builder()
                .income(500)
                .name("jose")
                .personCode("P033")
                .build();
        PersonResponse person1
                = restUtil.postForObject("/persons", applicant, PersonResponse.class);
        FundingRequestInput input = FundingRequestInput
                .builder()
                .applicantId(person1.getPersonId())
                .fundingAmount(5500)
                .vinNumber("123456789")
                .build();
        FundingRequestsResponse createdRequest
                = restUtil.postForObject(
                "/requests",
                input, FundingRequestsResponse.class);

        FundingRequestsResponse response
                = restUtil.getForObject(
                "/requests",
                FundingRequestsResponse.class, String.valueOf(createdRequest.getDecision().getRequestId()));

        assertEquals(FundingRequestStatus.REJECTED, response.getDecision().getRequestStatus());
    }
}
