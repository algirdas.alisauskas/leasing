package lt.carleasing.requests;

import lt.carleasing.CarLeasingApplicationTest;
import lt.carleasing.persons.Person;
import lt.carleasing.persons.PersonService;
import lt.carleasing.persons.model.ApplicantInput;
import lt.carleasing.requests.model.FundingRequest;
import lt.carleasing.requests.model.FundingRequestInput;
import lt.carleasing.requests.model.FundingRequestStatus;
import lt.carleasing.requirements.IncomesService;
import lt.carleasing.requirements.MinimalIncomeInput;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Transactional
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class FundingRequestsServiceIT extends CarLeasingApplicationTest {

    @Autowired
    FundingRequestsService service;

    @Autowired
    PersonService personService;

    @BeforeAll
    public static void init(@Autowired IncomesService incomesService) {
        incomesService
                .createMinimalIncome(
                        new MinimalIncomeInput(600));
    }

    @Test
    public void submit_ShouldReturnApprovedStatus() {
        Person mainApplicant = personService.createPersonAndSave(
                new ApplicantInput("Mike", 300, "CODE002"));
        Person coApplicant1 = personService.createPersonAndSave(
                new ApplicantInput("Jasmine", 400, "CODE001"));
        FundingRequestInput request = FundingRequestInput
                .builder()
                .applicantId(mainApplicant.getPersonId())
                .coApplicantsIds(Set.of(coApplicant1.getPersonId()))
                .fundingAmount(5500)
                .vinNumber("123456789")
                .build();

        FundingRequestStatus status = service.submit(request).getDecisionStatus();

        assertEquals(FundingRequestStatus.APPROVED, status);
    }

    @Test
    public void getFundingRequestById_ShouldReturnRequestWithCoApplicants() {
        Person mainApplicant = personService.createPersonAndSave(
                new ApplicantInput("Mike", 300, "CODE002"));
        Person coApplicant1 = personService.createPersonAndSave(
                new ApplicantInput("Jasmine", 400, "CODE001"));
        Person coApplicant2 = personService.createPersonAndSave(
                new ApplicantInput("Jose", 200, "CODE003"));
        FundingRequestInput request = FundingRequestInput
                .builder()
                .applicantId(mainApplicant.getPersonId())
                .coApplicantsIds(Set.of(
                        coApplicant1.getPersonId(),
                        coApplicant2.getPersonId()))
                .fundingAmount(5500)
                .vinNumber("123456789")
                .build();

        FundingRequest fundingRequest =
                service.getFundingRequestById(service.submit(request).getRequestId());

        assertEquals(2, fundingRequest.getCoApplicants().size());
    }

    @Test
    public void getFoundingRequestsByApplicant_ShouldReturnRequestList() {
        Person mainApplicant = personService.createPersonAndSave(
                new ApplicantInput("Mike", 300, "CODE002"));
        FundingRequestInput request1 = FundingRequestInput
                .builder()
                .applicantId(mainApplicant.getPersonId())
                .fundingAmount(5500)
                .vinNumber("123456789")
                .build();
        Person coApplicant1 = personService.createPersonAndSave(
                new ApplicantInput("Jasmine", 400, "CODE001"));
        Person coApplicant2 = personService.createPersonAndSave(
                new ApplicantInput("Jose", 200, "CODE003"));
        FundingRequestInput request2 = FundingRequestInput
                .builder()
                .applicantId(mainApplicant.getPersonId())
                .coApplicantsIds(Set.of(
                        coApplicant1.getPersonId(),
                        coApplicant2.getPersonId()))
                .fundingAmount(5500)
                .vinNumber("123456789")
                .build();
        service.submit(request1);
        FundingRequest requested = service.submit(request2);

        List<FundingRequest> requests
                = service.getFoundingRequestsByApplicant(requested.getApplicant());

        assertEquals(2, requests.size());
    }
}
